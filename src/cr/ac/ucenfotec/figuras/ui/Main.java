
package cr.ac.ucenfotec.figuras.ui;

import cr.ac.ucenfotec.figuras.bl.Circulo;
import cr.ac.ucenfotec.figuras.bl.Cuadrado;
import cr.ac.ucenfotec.figuras.bl.Rectangulo;

import java.io.*;
import java.util.ArrayList;

public class Main {
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    static ArrayList<Cuadrado> cuadrados = new ArrayList<>();
    static ArrayList<Circulo> circulos = new ArrayList<>();
    static ArrayList<Rectangulo> rectangulos = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        int opcion = 0;
        do {
            opcion = pintarMenu();
            switch (opcion) {
                case 1:
                    agregarCuadrado();
                    break;
                case 2:
                    verCuadrados();
                    break;
                case 3:
                    agregarCirculo();
                    break;
                case 4:
                    verCirculos();
                    break;
                case 5:
                    agregarRectangulo();
                    break;
                case 6:
                    verRectangulos();
                    break;
            }
        } while (opcion != 0);
    }

    public static int pintarMenu() throws IOException {
        out.println("--- MENU ---");
        out.println("1. Agregar Cuadrado");
        out.println("2. Ver Cuadrados");
        out.println("3. Agregar Circulo");
        out.println("4. Ver Circulos");
        out.println("5. Agregar Rectangulo");
        out.println("6. Ver Rectangulos");
        int opcion = Integer.parseInt(in.readLine());
        return opcion;
    }

    public static void agregarCuadrado() throws IOException {
        out.println("Agregar lado");
        out.println();
        out.print("Digite el lado: ");
        double lado = Double.parseDouble(in.readLine());
        Cuadrado pepito = new Cuadrado(lado);
        cuadrados.add(pepito);
    }

    public static void verCuadrados() {
        for (int i = 0; i < cuadrados.size(); i++) {
            Cuadrado cuadrado = cuadrados.get(i);
            out.println(i + ") " + cuadrado.toString());
            out.println("perimetro: " + cuadrado.calcularPerimetro());
            out.println("area: " + cuadrado.calcularArea());
            out.println("");
        }
    }
    public static void agregarCirculo() throws IOException {
        out.println("Agregar radio");
        out.println();
        out.print("Digite el radio: ");
        double radio = Double.parseDouble(in.readLine());
        Circulo pepito = new Circulo(radio);
        circulos.add(pepito);
    }

    public static void verCirculos() {
        for (int i = 0; i < circulos.size(); i++) {
            Circulo circulo = circulos.get(i);
            out.println(i + ") " + circulo.toString());
            out.println("perimetro: " + circulo.calcularPerimetro());
            out.println("area: " + circulo.calcularArea());
            out.println("");
        }
    }
    public static void agregarRectangulo() throws IOException {
        out.println("Agregar base");
        out.println();
        out.print("Digite la base: ");
        out.println("Agregar altura");
        out.println();
        out.print("Digite la altura: ");
        double altura = Double.parseDouble(in.readLine());
        double base = Double.parseDouble(in.readLine());
        Rectangulo pepito = new Rectangulo(altura, base);
        rectangulos.add(pepito);



    }


    public static void verRectangulos() {
        for (int i = 0; i < rectangulos.size(); i++) {
            Rectangulo rectangulo = rectangulos.get(i);
            out.println(i + ") " + rectangulo.toString());
            out.println("perimetro: " + rectangulo.calcularPerimetro());
            out.println("area: " + rectangulo.calcularArea());
            out.println("");
        }
    }
}