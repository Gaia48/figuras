package cr.ac.ucenfotec.figuras.bl;

public class Rectangulo {

    // ATRIBUTOS

    private double base;
    private double altura;


    // CONSTRUCTOR


    public Rectangulo(double base, double altura) {
        setBase(base);
        setAltura(altura);

    }



    // GETS & SETS

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }


    // FUNCIONES / METODOS / COMPORTAMIENTO

    public double calcularArea(){
        double area= getBase() * getAltura();
        return area;
    }

    public double calcularPerimetro(){
        double perimetro = 2 * (getBase()+getAltura());
        return perimetro;
    }
    // TOSTRING


    @Override
    public String toString() {
        return "Base: " + base + "  Altura: " + altura;

    }
}
