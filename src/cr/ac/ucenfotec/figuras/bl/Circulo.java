package cr.ac.ucenfotec.figuras.bl;

public class Circulo {

    // ATRIBUTOS

    private double radio;

    // CONSTRUCTOR

    public Circulo(double radio){
        setRadio(radio);
    }
    // GETS & SETS

    public double getRadio() {
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }

    // FUNCIONES / METODOS / COMPORTAMIENTO

    public double calcularArea (){
        double area = Math.PI * Math.pow(getRadio(),2);
        return area;
    }

    public double calcularPerimetro(){
        double perimetro = 2 * Math.PI * getRadio();
        return perimetro;
    }


    // TOSTRING

    @Override
    public String toString() {
        return "Radio: " + radio;
    }
}
