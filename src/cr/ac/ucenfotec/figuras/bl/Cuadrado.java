package cr.ac.ucenfotec.figuras.bl;

public class Cuadrado {
    // ATRIBUTOS
    private double lado;

    // CONSTRUCTOR
    public Cuadrado(double lado) {
        setLado(lado);
    }

    // GETS & SETS
    public double getLado() {
        return lado;
    }
    public void setLado(double lado) {
        this.lado = lado;
    }

    // FUNCIONES / METODOS / COMPORTAMIENTO
    public double calcularArea() {
        double area = getLado() * getLado();
        return area;
    }

    public double calcularPerimetro() {
        double perimetro = getLado() * 4;
        return perimetro;
    }

    // TOSTRING
    @Override
    public String toString() {
        return "Lado: " + lado;
    }
}